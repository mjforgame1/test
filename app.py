import httpx
from httpx import AsyncClient
from fastapi import FastAPI
from fastapi import Request
from fastapi.responses import StreamingResponse
from fastapi.responses import PlainTextResponse
from fastapi.background import BackgroundTasks
import json
import time
import gzip

class CachePool:
    def __init__(self, max_pool_size:int = 1024 * 1024 * 100, cache_time:float = 3600 * 3):
        self.pool:dict[str, bytes] = {}
        self.pool_headers = {}
        self.pool_time:dict[str, float] = {}
        self.pool_size:int = int(0)
        self.max_pool_size = max_pool_size
        self.cache_time = cache_time

    def update(self):
        curtime = time.time()
        for url, tm in self.pool_time.items():
            if curtime - tm > self.cache_time:
                file = self.pool.pop(url)
                self.pool_headers.pop(url)
                self.pool_time.pop(url)
                self.pool_size -= len(file)

    def cache(self, url:str, file:bytes, headers):
        if (self.pool_size + len(file) > self.max_pool_size):
            print('cache file is full, try to clean old files')
            self.update()

        if (self.pool_size + len(file) <= self.max_pool_size):
            #print('now cache file: ', url)
            self.pool[url] = file
            self.pool_headers[url] = headers
            self.pool_time[url] = time.time()
            self.pool_size += len(file)

    def get(self, url:str):
        tm = self.pool_time.get(url)
        if tm:
            if time.time() - tm > self.cache_time:
                file = self.pool.pop(url)
                self.pool_time.pop(url)
                self.pool_size -= len(file)
                return None, None
            else:
                #print('get cached file: ', url)
                return self.pool[url], self.pool_headers[url]
        return None, None

    def canbeCache(self, url: str):
        lowerstr = str.lower(url)
        pos = lowerstr.find('?')
        if pos > 0:
            lowerstr = lowerstr[0 : pos]
        return lowerstr.endswith('.js') or lowerstr.endswith('.css')

def bypass_headers(orgin_headers, bypass:list):
    h:dict[str,str] = {}
    for key in bypass:
        if orgin_headers.get(key):
            h[key] = orgin_headers[key]
    return h

app = FastAPI()
timeout = httpx.Timeout(60.0, connect=5.0)
limits = httpx.Limits(keepalive_expiry = 60)
HTTP_SERVER = AsyncClient(limits=limits, timeout=timeout, http2=False)
cachepool = CachePool()

@app.head("/api/{path:path}")
@app.options("/api/{path:path}")
@app.put("/api/{path:path}")
@app.patch("/api/{path:path}")
@app.delete("/api/{path:path}")
@app.get("/api/{path:path}")
@app.post("/api/{path:path}")
async def run(path: str, request: Request):
    base_url = "https://www.baidu.com/"
    if request.headers.get('visit-url'):
        url = request.headers.get('visit-url')
    else:
        url = path + '?' + str(request.query_params)
        if not (len(url) > 4 and url[0] == 'h' and url[1] == 't' and url[2] == 't' and url[3] == 'p'):
            url = base_url + url
    #print("request url:", url)
    use_orgin_header = True if request.headers.get('origin-header') else False
    if use_orgin_header:
        h = dict(json.loads(request.headers.get('origin-header')))
    else:
        h = dict(request.headers)
        h.pop('host', None)
    # httpx支持br压缩格式，如果需要的话，可以显式声明一下 
    #if h.get('accept-encoding'):
    #    h['accept-encoding'] = 'gzip, deflate, br'
    if request.headers.get('custom-ip'):
        h['x-real-ip'] = request.headers.get('custom-ip')
        h['x-forwarded-for'] = request.headers.get('x-forwarded-for')
        h.pop('custom-ip', None)

    url_for_cache = None
    if request.method.lower() == 'get' and cachepool.canbeCache(url):
        url_for_cache = url
        file, cache_headers = cachepool.get(url_for_cache)
        if file:
            return PlainTextResponse(file, headers=cache_headers)
    try:
        rp_req = HTTP_SERVER.build_request(
            request.method, url, headers = h
            ,content = await request.body()
        )
        #print(rp_req.headers)
        rp_resp = await HTTP_SERVER.send(rp_req, stream=True, follow_redirects=False)
        #h = dict(rp_resp.headers)
        #去掉返回的 Content-Length 头。因为 StreamingResponse 会自己定义返回长度
        #h.pop('content-length', None)
        #由于可能会返回多条set-cookie，而dict会将它们合为一条，用','进行分割
        #然而数据中本来就可能含有','，例如日期。因此set-cookie必须拿出来单独处理
        #h.pop('set-cookie', None)

        #仅保留与客户端通信所必须的部分headers
        h = bypass_headers(rp_resp.headers, bypass=['connection', 'content-encoding'])
        #由于有的服务器会尝试以文本形式自动解析，导致出错，因此将类型设为压缩文件，使服务器放弃解析
        h['content-type'] = 'application/octet-stream'
        #记录原始的响应头，以特殊方式传给客户端
        origin_header = dict(rp_resp.headers)
        origin_header.pop('set-cookie', None)
        h['origin-header'] = json.dumps(origin_header)
        if url_for_cache and (rp_resp.status_code == 200):
            h['content-encoding'] = 'gzip'
            content = gzip.compress(await rp_resp.aread())
            cachepool.cache(url_for_cache, file = content, headers = h)
            response = PlainTextResponse(content, status_code=rp_resp.status_code,headers=h,background=BackgroundTasks([rp_resp.aclose]))
        else:
            #不支持200以下的状态码，这些状态码通常用于ws之类的协议切换
            status_code = 200 if rp_resp.status_code < 200 else rp_resp.status_code
            response = StreamingResponse(rp_resp.aiter_raw(1024 * 100), status_code=status_code,headers = h,background=BackgroundTasks([rp_resp.aclose]))

        if rp_resp.headers.get('set-cookie'):
            cooks_dict = []
            for key,val in rp_resp.headers.multi_items():
                if key == 'set-cookie':
                    cooks_dict.append(val)
            #由于有些服务器无法发送多条set-cookie，会吞掉。因此加入一个custom-cookies头，用特殊的办法传给客户端
            response.headers.append('custom-cookies', json.dumps(cooks_dict))
        return response
    except Exception as e:
        return PlainTextResponse("服务器发生错误！" + str(e), status_code = 400)

@app.get("/healthz")
@app.get("/")
async def welcome():
    return PlainTextResponse("Hello World")


from starlette.websockets import WebSocket
import asyncio
import websockets

async def server_to_client(websocket: WebSocket, server_websocket):
    try:
        async for msg in server_websocket:
            if type(msg) == bytes:
                await websocket.send_bytes(msg)
            else:
                await websocket.send_text(msg)
    except Exception as e:
        print("Websocket Exception: ", e)
    finally:
        await server_websocket.close()
        try:
            await websocket.close()
        except Exception as e:
            return

@app.websocket("/api/{path:path}")
async def websocket_endpoint(websocket: WebSocket):
    if websocket.headers.get('visit-url') and websocket.headers.get('origin-header'):
        url = websocket.headers.get('visit-url')
        h = dict(json.loads(websocket.headers.get('origin-header')))
        subprotocols = None
        if h.get('sec-websocket-protocol'):
            subprotocols = [x.strip() for x in h['sec-websocket-protocol'].split(',')]
        origin = h.get('origin')
        h.pop('origin')
        h.pop('sec-websocket-key', None)
        h.pop('sec-websocket-version', None)
        h.pop('sec-websocket-extensions', None)
        h.pop('sec-websocket-protocol', None)
        h.pop('host', None)
        h.pop('connection', None)
        h.pop('upgrade', None)
    else:
        return
    server_websocket = None

    try:
        server_websocket = await websockets.connect(url, extra_headers = h, subprotocols = subprotocols, origin = origin)
        await websocket.accept(server_websocket.subprotocol)
        asyncio.create_task(server_to_client(websocket, server_websocket))
        while True:
            msg = await websocket.receive()
            if (msg.get("text")):
                await server_websocket.send(msg["text"])
            elif (msg.get("bytes")):
                await server_websocket.send(msg["bytes"])
    except Exception as e:
        print("Websocket Exception: ", e)
    finally:
        if server_websocket:
            await server_websocket.close()
        try:
            await websocket.close()
        except Exception as e:
            return

if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host = '0.0.0.0', port = 7860, log_level='info', timeout_keep_alive = 60, server_header=False, date_header=False)
    
